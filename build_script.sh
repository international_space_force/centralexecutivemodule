echo Building: Central Executive Module
g++ -pthread Driver.cpp -o cenexecmod
chmod +x cenexecmod
echo Done building, moving to rover executables...
cp ./cenexecmod ../roverexecutables/cenexecmod
echo Done moving, exiting...

