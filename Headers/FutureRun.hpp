#ifndef CENTRALEXECUTIVEMODULE_FUTURERUN_HPP
#define CENTRALEXECUTIVEMODULE_FUTURERUN_HPP

#include <functional>
#include <chrono>
#include <future>
#include <cstdio>

class FutureRun {
public:

    // Calling format:
    // <time to wait in ms>, <if run asynchronous>, <&function>[, <parameters list for function>]
    template <class callable, class... arguments>
    FutureRun(int after, bool async, callable&& f, arguments&&... args) {
        std::function<typename std::result_of<callable(arguments...)>::type()> task(std::bind(std::forward<callable>(f),
                                                                                              std::forward<arguments>(args)...));
        if (async) {
            std::thread([after, task]() {
                std::this_thread::sleep_for(std::chrono::milliseconds(after));
                task();
            }).detach();
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(after));
            task();
        }
    }
};

#endif //CENTRALEXECUTIVEMODULE_FUTURERUN_HPP
