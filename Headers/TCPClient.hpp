#ifndef CCSCOMMUNICATIONMODULE_TCPCLIENT_HPP
#define CCSCOMMUNICATIONMODULE_TCPCLIENT_HPP

#include <string>
#include <netinet/in.h>
#include <arpa/inet.h>

class TCPClient {
private:
    int remoteServerPort;
    std::string remoteServerAddress;
    struct sockaddr_in server_address;
    int sock;

    void InitializeConnection() {
        memset(&server_address, 0, sizeof(server_address));
        server_address.sin_family = AF_INET;
        inet_pton(AF_INET, remoteServerAddress.c_str(), &server_address.sin_addr);
        server_address.sin_port = htons(remoteServerPort);
    }

    bool ConnectionIsOpen() {
        bool result = true;
        if((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
            printf("Could not create Rover com socket\n");
            result = false;
        }

        if (result && connect(sock, (struct sockaddr*)&server_address, sizeof(server_address)) < 0) {
            printf("Could not connect to Rover\n");
            result = false;
        }

        return result;
    }

public:
    TCPClient(std::string remoteAddress, int remotePort) {
        remoteServerAddress = remoteAddress;
        remoteServerPort = remotePort;

        InitializeConnection();

        ConnectionIsOpen();
    }

    bool SentDataSuccessfully(char* dataToSend, int bytesToSend) {
        bool result = false;
        if(send(sock, dataToSend, bytesToSend, 0) >= 0) {
            result = true;
        }
        return result;
    }

    void ReceiveDataFromServer(int bufferLimit, char* buffer, int& receivedByteCount) {
        //receivedByteCount = recvfrom(sock, buffer, bufferLimit, 0, nullptr, nullptr);

        while(receivedByteCount == 0) {
            receivedByteCount = recv(sock, buffer, bufferLimit, 0);
        }
    }

};

#endif //CCSCOMMUNICATIONMODULE_TCPCLIENT_HPP
