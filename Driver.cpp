#include "Headers/ConcurrentQueue.hpp"
#include "Headers/UDPServer.hpp"
#include "Headers/ModuleDesignations.hpp"
#include "Headers/ConcurrentFileLogger.hpp"
#include "Headers/UDPClient.hpp"
#include "MAVLink/common/mavlink.h"
#include <atomic>
#include "unistd.h"
#include "Headers/ConcurrentMap.hpp"
#include "Headers/FutureRun.hpp"
#include "Headers/MessageCreationSend.hpp"
#include "Headers/MessageRetry.hpp"

const int DATA_LIMIT_SIZE = 3000;
const int DEFAULT_ACK_WAIT_TIME = 5000;
const int HEARTBEAT_STATUS_INTERVAL_IN_SECS = 12;
const std::string LOOPBACK_ADDRESS = "127.0.0.1";

ConcurrentFileLogger logger("CentralExecModuleLog.txt");
std::atomic<bool> inManualMode;
std::atomic<bool> inKillSwitchMode;

ConcurrentQueue<std::tuple<float, float>> coordinatesFromOperator;

// Key: MsgChecksum, Value: Number of responses waiting for
ConcurrentMap<uint16_t, int> messagesToBeReturnedRetryCount;

// Key: MsgChecksum, Value: messageByteData
ConcurrentMap<uint16_t, uint8_t[]> messagesToBeReturnedByteData;

[[noreturn]] void HandleIncomingData(UDPServer& serverConnection, ConcurrentQueue<std::vector<char>>& dataReceived) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;

        //printf("Waiting for local data: \n");
        serverConnection.GetDatagram(std::ref(receivedByteCount), receivedBuffer, DATA_LIMIT_SIZE);
        //printf("Received local data: %s\n", receivedBuffer);

        dataReceived.push(CharArrayToVector(receivedBuffer, receivedByteCount));
    }
}

[[noreturn]] void SendOutGoingData(UDPClient& clientModuleConnection, ConcurrentQueue<std::vector<char>>& queue) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int sendingByteCount = 0;
        std::vector<char> dataToBeSent;

        queue.pop(dataToBeSent);
        sendingByteCount = dataToBeSent.size();

        VectorToCharArray(dataToBeSent, receivedBuffer, DATA_LIMIT_SIZE);

        //printf("Outgoing Data\n");
        clientModuleConnection.SentDatagramToServerSuccessfully(receivedBuffer, sendingByteCount);
    }
}

void CheckIfExpectedMsgReceived(uint16_t expectedMsgChecksum) {
    int currentMsgWaitCount = messagesToBeReturnedRetryCount.GetElementAt(expectedMsgChecksum);
    if(currentMsgWaitCount == 0) {
        // Expected message was received
        RemoveMsgToBeWaitedOn(messagesToBeReturnedRetryCount, expectedMsgChecksum);
    } else {
        // Expected message was not received
        DecrementMsgWaitResponse(messagesToBeReturnedRetryCount, expectedMsgChecksum);
        FutureRun waitForMessage(5000, true, &CheckIfExpectedMsgReceived, expectedMsgChecksum);
    }
}

void AddFutureWaitForMessage(uint16_t expectedMsgChecksum) {
    AddNewMsgToWaitForResponse(messagesToBeReturnedRetryCount, expectedMsgChecksum);
    FutureRun waitForMessage(DEFAULT_ACK_WAIT_TIME, true, &CheckIfExpectedMsgReceived, expectedMsgChecksum);
}

void DidReceiveExpectedMessage(uint16_t expectedMsgChecksum) {
    SetKeyToZero(messagesToBeReturnedRetryCount, expectedMsgChecksum);
}

void ParseCmdInt(mavlink_command_int_t mavCmdInt, ConcurrentQueue<std::vector<char>>& outgoingData) {
    uint16_t msgChecksum = 0;
    float latitude = 0;
    float longitude = 0;

    switch (mavCmdInt.command) {

        case MAV_CMD_NAV_WAYPOINT:
            // Store navigate to coordinate point from operator
            latitude = mavCmdInt.param1;
            longitude = mavCmdInt.param2;
            coordinatesFromOperator.push(std::tuple<float, float>(latitude, longitude));
            // Pass along location to navigate to path planning module
            msgChecksum = SendWaypointLocation(outgoingData, CentralExecutive, PathPlanning,
                    latitude, longitude, DATA_LIMIT_SIZE, logger);
            AddFutureWaitForMessage(msgChecksum);
            break;

        case MAV_CMD_DO_FLIGHTTERMINATION:
            // Received kill switch status update
            inKillSwitchMode = (mavCmdInt.param1 == 1);
            // Pass on to motor control
            msgChecksum = SendKillSwitchStatus(outgoingData, inKillSwitchMode, CentralExecutive,
                                               MotorControl, DATA_LIMIT_SIZE, logger);
            AddFutureWaitForMessage(msgChecksum);
            break;

        case MAV_CMD_DO_PAUSE_CONTINUE:
            // Received go/stop cmd for mission, pass on central exec
            msgChecksum = SendStopGoCommand(outgoingData, (mavCmdInt.param1 == 1), CentralExecutive,
                    MotorControl, DATA_LIMIT_SIZE, logger);
            AddFutureWaitForMessage(msgChecksum);
            break;

        case MAV_CMD_DO_SET_MODE:
            inManualMode = (mavCmdInt.param1 == MAV_MODE_MANUAL_ARMED);
            msgChecksum = SendManualModeStatus(outgoingData, inManualMode, CentralExecutive,
                                               MotorControl, DATA_LIMIT_SIZE, logger);
            AddFutureWaitForMessage(msgChecksum);
            break;

        default:
            // Log unknown message with time ********************
            logger.WriteToFile("Invalid CMD INT MAVLink Data Received");
            break;
    }
}

void ParseCmdLong(__mavlink_command_long_t mavCmdLong, ConcurrentQueue<std::vector<char>>& outgoingData) {

}

[[noreturn]] void ParseReceivedData(ConcurrentQueue<std::vector<char>>& messagesToParse,
                                    ConcurrentQueue<std::vector<char>>& outgoingData) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        uint8_t sendBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;
        std::vector<char> dataToBeSent;

        messagesToParse.pop(dataToBeSent);
        receivedByteCount = dataToBeSent.size();
        VectorToCharArray(dataToBeSent, receivedBuffer, DATA_LIMIT_SIZE);

        mavlink_message_t msg;
        mavlink_status_t status;
        unsigned int firstCharInBuffer = -1;
        bool dataWasCorrectlyParsed = false;

        for (int bufferIndex = 0; bufferIndex < receivedByteCount; ++bufferIndex) {
            firstCharInBuffer = dataToBeSent[bufferIndex];
            //printf("%02x ", (unsigned char)firstCharInBuffer);

            if (mavlink_parse_char(MAVLINK_COMM_0, dataToBeSent[bufferIndex], &msg, &status)) {
                uint8_t buffer[DATA_LIMIT_SIZE];
                logger.WriteToFile(MAVLinkMsgToStringStream(msg, true));
                dataWasCorrectlyParsed = true;
                int sizeInBytes = 0;
                uint16_t msgChecksum = 0;

                switch (msg.msgid) {

                    case MAVLINK_MSG_ID_COMMAND_INT:
                        mavlink_command_int_t mavCmdInt;
                        mavlink_msg_command_int_decode(&msg, &mavCmdInt);
                        ParseCmdInt(mavCmdInt, std::ref(outgoingData));
                        break;

                    case MAVLINK_MSG_ID_COMMAND_LONG:
                        __mavlink_command_long_t mavCmdLong;
                        mavlink_msg_command_long_decode(&msg, &mavCmdLong);
                        ParseCmdLong(mavCmdLong, outgoingData);
                        break;

                    case MAVLINK_MSG_ID_DISTANCE_SENSOR:
                        // Got detection from ultrasonic module
                        logger.WriteToFile("Ultrasonic Detection Received");
                        SendAckBack(outgoingData, CentralExecutive, UltrasonicSensor, msg.checksum,
                            MAVLINK_MSG_ID_DISTANCE_SENSOR, 0, DATA_LIMIT_SIZE, logger);

                        if(!inManualMode) {
                            inManualMode = true;
                            // Send to Motor Control Module to stop
                            msgChecksum = SendCollisionMessage(outgoingData, true,
                                    CentralExecutive,MotorControl, DATA_LIMIT_SIZE, logger);
                            AddFutureWaitForMessage(msgChecksum);
                        }

                        // Send to Mission Executive to notify operator
                        msgChecksum = SendCollisionMessage(outgoingData, true,
                                                           CentralExecutive,MissionExecutive,
                                                           DATA_LIMIT_SIZE, logger);
                        AddFutureWaitForMessage(msgChecksum);
                        break;

                    case MAVLINK_MSG_ID_HEARTBEAT:
                        //logger.WriteToFile("Heartbeat Status Received");
                        break;

                    case MAVLINK_MSG_ID_RAW_PRESSURE:
                        // Received environment data, forward to GUI
                        mavlink_raw_pressure_t rawPressureMsg;
                        mavlink_msg_raw_pressure_decode(&msg, &rawPressureMsg);
                        msgChecksum = SendEnvironmentalDataMsg(outgoingData, CentralExecutive, GUI,
                                rawPressureMsg.press_abs, rawPressureMsg.press_diff1, rawPressureMsg.temperature,
                                DATA_LIMIT_SIZE, logger);
                        AddFutureWaitForMessage(msgChecksum);
                        break;

                    case MAVLINK_MSG_ID_LOCAL_POSITION_NED:
                        // Forward accelerometer data to GUI
                        mavlink_local_position_ned_t accelerometerMsg;
                        mavlink_msg_local_position_ned_decode(&msg, &accelerometerMsg);
                        msgChecksum = SendAccelerometerDataMsg(outgoingData, CentralExecutive, GUI,
                                DATA_LIMIT_SIZE, accelerometerMsg.time_boot_ms, accelerometerMsg.x, accelerometerMsg.y,
                                accelerometerMsg.z, logger);
                        AddFutureWaitForMessage(msgChecksum);
                        break;

                    case MAVLINK_MSG_ID_LANDING_TARGET:
                        // Forward remaining distance from Path Planning to Motor Control
                        mavlink_landing_target_t distanceToTargetMessage;
                        mavlink_msg_landing_target_decode(&msg, &distanceToTargetMessage);

                        if(msg.sysid == 255) {
                            // From path planning module, send to mission exec and motor control
                            msgChecksum = SendDistanceToLocationMsg(outgoingData, CentralExecutive,
                                    MotorControl, DATA_LIMIT_SIZE, distanceToTargetMessage, logger);
                            AddFutureWaitForMessage(msgChecksum);
                            // Send remaining distance to mission exec for GUI
                            msgChecksum = SendDistanceToLocationMsg(outgoingData, CentralExecutive,
                                                                    MissionExecutive, DATA_LIMIT_SIZE,
                                                                    distanceToTargetMessage, logger);
                            AddFutureWaitForMessage(msgChecksum);
                        } else if(msg.sysid == MissionExecutive) {
                            // Received new location to navigate to
                            // Save new location

                            // Send location to path planning module

                        }
                        break;

                    case MAVLINK_MSG_ID_COMMAND_ACK:
                        // Handle responses from other modules
                        mavlink_command_ack_t ackResponse;
                        mavlink_msg_command_ack_decode(&msg, &ackResponse);
                        DidReceiveExpectedMessage(msg.checksum);
                        break;

                    case MAVLINK_MSG_ID_MISSION_CURRENT:
                        // Received IED detection
                        mavlink_mission_current_t iedDetection;
                        mavlink_msg_mission_current_decode(&msg, &iedDetection);

                        if(!inManualMode && iedDetection.seq == 1) {
                            // Put in manual mode
                            inManualMode = true;

                            // Tell Rover to stop
                            msgChecksum = SendCollisionMessage(outgoingData, false,
                                                               CentralExecutive,MotorControl, DATA_LIMIT_SIZE,
                                                               logger);
                            AddFutureWaitForMessage(msgChecksum);
                        }

                        msgChecksum = SendManualModeStatus(outgoingData, inManualMode, CentralExecutive,
                                                           MotorControl, DATA_LIMIT_SIZE, logger);
                        AddFutureWaitForMessage(msgChecksum);

                        // Notify operator
                        msgChecksum = SendIEDDetectionMessage(outgoingData, CentralExecutive,
                                MissionExecutive,iedDetection.seq == 1, DATA_LIMIT_SIZE, logger);
                        break;

                    case MAVLINK_MSG_ID_MISSION_ITEM_REACHED:
                        // Received at dest msg from path planning
                        // Tell Rover to stop
                        msgChecksum = SendCollisionMessage(outgoingData, false,
                                                           CentralExecutive,MotorControl, DATA_LIMIT_SIZE,
                                                           logger);
                        AddFutureWaitForMessage(msgChecksum);

                        // Pass arrived indication to mission exec
                        mavlink_mission_item_reached_t locationReached;
                        mavlink_msg_mission_item_reached_decode(&msg, &locationReached);
                        msgChecksum = SendLocationReachedMessage(outgoingData, CentralExecutive, MissionExecutive,
                                                                 locationReached.seq == 1, DATA_LIMIT_SIZE,
                                                                 logger);
                        AddFutureWaitForMessage(msgChecksum);

                        // Put in manual mode
                        inManualMode = true;
                        msgChecksum = SendManualModeStatus(outgoingData, inManualMode, CentralExecutive,
                                                           MotorControl, DATA_LIMIT_SIZE, logger);
                        AddFutureWaitForMessage(msgChecksum);

                        // Remove coordinate from list
                        if(coordinatesFromOperator.size() > 0) {
                            coordinatesFromOperator.pop();
                        }
                        break;

                    default:
                        // Log unknown message with time ********************
                        logger.WriteToFile("Invalid MAVLink Data Received\n");
                        break;
                }
            }
        }
        if(!dataWasCorrectlyParsed) {
            logger.WriteToFile("Invalid Data Received\n");
        }
    }
}

[[noreturn]] void SendHeartbeatStatusMessage(ConcurrentQueue<std::vector<char>>& dataQueue) {
    while(true) {
        /* Send Heartbeat */
        uint8_t buf[DATA_LIMIT_SIZE];
        mavlink_message_t msg;
        uint16_t len;
        mavlink_msg_heartbeat_pack(CentralExecutive, RoverCommunication, &msg,
                MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC, MAV_MODE_GUIDED_ARMED,
                0, MAV_STATE_ACTIVE);
        len = mavlink_msg_to_send_buffer(buf, &msg);

        std::vector<char> dataToSend = CharArrayToVector((char*)buf, len);
        dataQueue.push(dataToSend);

        // Sleep for X seconds
        sleep(HEARTBEAT_STATUS_INTERVAL_IN_SECS);
    }
}

int main(int argc, char *argv[]) {
    // Data structures for handling local data
    ConcurrentQueue<std::vector<char>> incomingDataToParse;
    ConcurrentQueue<std::vector<char>> outgoingData;
    ConcurrentQueue<float> distanceToDestination;

    // UDP data connections
    UDPServer thisServer(CentralExecutiveModule, LOOPBACK_ADDRESS);
    UDPClient connectionToRoverCommunicationModule(RoverCommunicationModule, LOOPBACK_ADDRESS);

    // Data connections
    std::thread handleIncomingData(HandleIncomingData, std::ref(thisServer), std::ref(incomingDataToParse));
    std::thread sendOutgoingData(SendOutGoingData, std::ref(connectionToRoverCommunicationModule),
            std::ref(outgoingData));

    // Module logic execution
    std::thread handleReceivedDataMessages(ParseReceivedData, std::ref(incomingDataToParse),
            std::ref(outgoingData));
    std::thread sendPeriodicHeartbeatMsg(SendHeartbeatStatusMessage, std::ref(outgoingData));

    printf("Central Executive Module Executing\n");

    //******************FOR TESTING****************************************************



    //******************FOR TESTING****************************************************

    handleIncomingData.join();
    sendOutgoingData.join();
    handleReceivedDataMessages.join();
    sendPeriodicHeartbeatMsg.join();

    return 0;
}
